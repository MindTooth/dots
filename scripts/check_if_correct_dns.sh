#!/bin/sh

TITLE="Missing 127.0.0.1 entry!"
MESSAGE="Entry for 127.0.0.1 is not present in the DNS list, this is
vital for DNSCrypt to resolve local requests."

if ! (networksetup -getdnsservers Wi-Fi | grep -q "127.0.0.1"); then
  osascript -e "display alert \"$TITLE\" message \"$MESSAGE\""
fi
