#!/bin/sh
# TODO:

llvm_path="$(brew --prefix llvm)"

if [ ! -e "$llvm_path" ]; then
	echo "$llvm_path is empty"
	exit 1
fi

if [ -z "$1" ]; then
	echo "Need to accept with -y"
	exit
fi

link_exec() {
	ln -s "$1" "$llvm_path/bin/clang-format" "/usr/local/bin/clang-format"
	ln -s "$1" "$llvm_path/bin/clang-tidy" "/usr/local/bin/clang-tidy"
	ln -s "$1" "$llvm_path/share/clang/run-clang-tidy.py" "/usr/local/bin/run-clang-tidy.py"
}

force_link=""

while getopts "yf" o; do
	case "${o}" in
	y) ;;

	f)
		force_link="f"
		;;
	*)
		echo "Invalid option: Pass -y for linking"
		exit
		;;
	esac
done

ln -s"$force_link" "$llvm_path/bin/clang-format" "/usr/local/bin/clang-format"
ln -s"$force_link" "$llvm_path/bin/clang-tidy" "/usr/local/bin/clang-tidy"
ln -s"$force_link" "$llvm_path/share/clang/run-clang-tidy.py" "/usr/local/bin/run-clang-tidy.py"
