# Load all envfiles

# TODO:
# * make queries more effective
# * prettier output
# * look at switching to a switch
# * create a boxprintbox function
# * convert to sh/bash for portability
# * possibal to unset?

# Completion for loadsecrets
complete -x -c loadsecrets -a "(ls $HOME/.secrets | string replace -r '.[^.]*\$' '')"
complete -x -c loadsecrets -l list -d "List all secrets"
complete -x -c loadsecrets -l all -d "Source all secrets"

# loadsecrets Sourcing secret files from the .secrets folder
function loadsecrets -d "Source all secrets inside the .secrets filer"
  # Setting some variables
  set secret_folder "$HOME/.secrets"
  set secret_files (ls -1 $secret_folder | grep ".env")
  set secret_list (string replace -r '.[^.]*$' '' $secret_files)

  # List all possible secrets
  if contains -- --list $argv
    for secret in $secret_list
      echo $secret
    end
    return 0
  end

  # Sources all the secrets
  if contains -- --all $argv
    for secret in $secret_files
      echo "Sources the file: $secret"
      source "$secret_folder/$secret"
    end
    return 0
  end

  if contains $argv $secret_list
    set file "$secret_folder/$argv.env"

    printf "
  ################
  #
  #\t%s
  #
  ###############\n" \
      (head -n1 $file | string replace '# ' '')

    printf "\n  The folowing keys are exported:\n\n"
    for variable in (cat $file | string match 'export*' | string replace -r '^export\s(\w+)=.+$' '$1')
      printf "  * %s\n" $variable
    end

    ## Sourcing the given file
    source $file
  else
    echo "Can't load secret..."
  end
end
