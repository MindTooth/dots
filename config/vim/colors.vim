" colors.vim - Color Settings

" Initial Settings {{{

" Terminal.app don't support 24-bit colors
if (exists('+termguicolors') && $TERM_PROGRAM !=? 'Apple_Terminal')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif

" }}}

" Srcery Settings {{{

let g:srcery_italic = 1

" }}}

" Set Colorscheme {{{

set background=dark
colorscheme srcery

" }}}

" Airline Settings {{{

let g:airline_powerline_fonts = 1
let g:airline#extensions#ale#enabled = 1

" }}}

" vim: filetype=vim:
