" Check if xdg is cache is set
let s:cache_path = !empty($XDG_CACHE_HOME) ? $XDG_CACHE_HOME : expand('$HOME/.cache')

if exists('*mkdir') && !empty(s:cache_path) && !isdirectory(s:cache_path . '/vim')
    for g:dir in ['vim', 'vim/backup', 'vim/swap', 'vim/undo']
        if !isdirectory(s:cache_path . '/' .  g:dir)
            call mkdir(s:cache_path . '/' . g:dir)
        endif
    endfor
endif

" Set directories
let &backupdir  = s:cache_path . '/vim/backup//'
set backup

let &directory  = s:cache_path . '/vim/swap//'
set swapfile

let &undodir    = s:cache_path . '/vim/undo//'
set undofile
let &undolevels = 1000
let &undoreload = 10000

" Disabled for regular Vim as they both can't write to the same vimfile.
if has('nvim')
  let &viminfo    = "!,'1000,<50,s100,h,n" . s:cache_path . '/vim/nviminfo'
else
  let &viminfo    = "!,'1000,<50,s100,h,n" . s:cache_path . '/vim/viminfo'
endif

" vim: set filetype=vim:
