" mindtooth.vim
"
" My own user functions.

function! mindtooth#dein_recache() abort
  echomsg string('Refreshing Dein')
  "call map(dein#check_clean(), '"delete(".v:val.", \"rf\")"')
  call map(dein#check_clean(), "delete(v:val, 'rf')")


  if dein#check_install()
    call dein#install()
  endif

  call dein#recache_runtimepath()
endfunction

" vim: set ft=vim et sts=2 sw=2 tw=2 :
