function! s:ipynb()
	setfiletype ipynb
endfunction

autocmd BufNewFile,BufRead *.ipynb,*.IPYNB call s:ipynb()

" vim: set filetype=vim :
