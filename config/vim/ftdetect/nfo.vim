" nfo.vim - detect the nfo file format
function! s:nfo()
	if &enc != 'cp437' || ft != 'nfo'
		silent edit ++enc=cp437
		setfiletype nfo
	end
endfunction
autocmd BufNewFile,BufRead *.nfo,*.NFO call s:nfo()

" vim: set filetype=vim:
