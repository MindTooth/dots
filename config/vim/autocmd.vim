" autocommands.vim

if has('autocmd')

    augroup ipa_at_load
        autocmd!

        autocmd VimEnter * silent call ToggleIPA()
    augroup END

    augroup toggleRelativeLineNumbers
        autocmd!

        autocmd InsertEnter,BufLeave,WinLeave,FocusLost * nested
                    \ if &l:number && empty(&buftype) |
                    \ setlocal norelativenumber |
                    \ endif
        autocmd InsertLeave,BufEnter,WinEnter,FocusGained * nested
                    \ if &l:number && empty(&buftype) |
                    \ setlocal relativenumber |
                    \ endif
    augroup END

    augroup VimReload
        autocmd!

        autocmd BufWritePost $MYVIMRC nested source $MYVIMRC | :call RefreshUI()
    augroup END

endif

" vim: filetype=vim:
