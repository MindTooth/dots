" functions.vim
scriptencoding utf-8

" vim-airline {{

" https://github.com/vim-airline/vim-airline/issues/312#issuecomment-68887069
function! RefreshUI() abort
    if exists(':AirlineRefresh')
	AirlineRefresh
    else
	" Clear & redraw the screen, then redraw all statuslines.
	redraw!
	redrawstatus!
    endif
endfunction

" }}
" {{ IPA

let g:IPAOn=0

function! ToggleIPA()
    if !g:IPAOn
        call IPA()
    else
        call IPAOff()
    endif
endfunction

function! IPA()
    echo 'IPA macros activated'
    let g:IPAOn=1
    imap ;ae æ
    imap ;oe ø
    imap ;aa å
    imap ;AE Æ
    imap ;OE Ø
    imap ;AA Å
    imap ;ee é
endfunction

function! IPAOff()
    echo 'IPA macros deactivated'
    let g:IPAOn=0
    iunmap ;ae
    iunmap ;oe
    iunmap ;aa
    iunmap ;AE
    iunmap ;OE
    iunmap ;AA
    iunmap ;ee
endfunction

" }}
" {{ Testing

function! BreakHabitsWindow() abort
    " Define the size of the floating window
    let width = 50
    let height = 10

    " Create the scratch buffer displayed in the floating window
    let buf = nvim_create_buf(v:false, v:true)

    " Get the current UI
    let ui = nvim_list_uis()[0]

    " Create the floating window
    let opts = {'relative': 'editor',
                \ 'width': width,
                \ 'height': height,
                \ 'col': (ui.width/2) - (width/2),
                \ 'row': (ui.height/2) - (height/2),
                \ 'anchor': 'NW',
                \ 'style': 'minimal',
                \ }
    let win = nvim_open_win(buf, 1, opts)

    " create the lines to draw a box
    let horizontal_border = '+' . repeat('-', width - 2) . '+'
    let empty_line = '|' . repeat(' ', width - 2) . '|'
    let lines = flatten([horizontal_border, map(range(height-2), 'empty_line'), horizontal_border])

    " set the box in the buffer
    call nvim_buf_set_lines(buf, 0, -1, v:false, lines)
endfunction

" vim: set filetype=vim :
