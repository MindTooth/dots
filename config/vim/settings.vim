" settings.vim - Vim Settings

set noshowmode                  " Disable the default mode line
set number                      " Display numbers
set cursorline                  " show where the cursor is
set title                       " Set xterm title
set hidden                      " Allow background buffers without writing them, and remember marks/undo for background buffers
set cpoptions+=$                " Set dollar sign as change tag
" Whitespace
set nowrap                      " don't wrap lines
set list                        " Show invisible characters
" Searching
set hlsearch                    " highlight matches
set ignorecase                  " searches are case insensitive...
set smartcase                   " ... unless they contain at least one capital letter
" Visual
" set textwidth=72                " set width to 80 char
set colorcolumn=+1              " red line and over is error
" set formatoptions-=r            " Set wrap setting
" set formatoptions=c            " Set wrap setting
" set formatoptions=cro            " Set wrap setting
set ttyfast                     " Fast terminal
set lazyredraw                  " Don't redraw uneccessary
set scrolloff=5                 " Set scrolloff distance

"set iskeyword+=-                " Treat dash as word object

" Enable spelling
" setlocal spell
set spelllang=nb,en_us
" inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u

" Make use of two spaces between sentances.
" http://stevelosh.com/blog/2012/10/why-i-two-space/
set cpoptions+=J

" Split differently
" http://robots.thoughtbot.com/post/48275867281/vim-splits-move-faster-and-more-naturally
set splitbelow
set splitright

" Copy/paste macoS
set clipboard=unnamed

" Use `ripgrep` as `grep`
if executable('rg')
    set grepprg=rg\ --no-heading\ --vimgrep
    set grepformat=%f:%l:%c:%m
endif

if has('nvim')
  let g:python_host_prog = '/usr/bin/python2'
  let g:python3_host_prog = '/usr/bin/python3'
endif

" vim: set filetype=vim:
