" macvim.vim - MacVim

set guifont=JetBrainsMono-Regular:h12
set transparency=5

set macligatures

" vim: set filetype=vim :
