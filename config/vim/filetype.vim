" OpenTofu

augroup add_custom_filetypes
    autocmd!
    autocmd BufNewFile,BufRead *.tofu set filetype=terraform
augroup end
