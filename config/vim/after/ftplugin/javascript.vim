" after/ftplugin/js.vim

setlocal expandtab
setlocal shiftwidth=2
setlocal softtabstop=2

" vim: set filetype=vim:
