call deoplete#custom#buffer_option('auto_complete', v:false)

imap <silent><buffer> <Esc> <Plug>(denite_filter_quit)
inoremap <silent><buffer><expr> <C-j> denite#incremant_parent_cursor(1)
inoremap <silent><buffer><expr> <C-k> denite#incremant_parent_cursor(-1)
nnoremap <silent><buffer><expr> <C-j> denite#incremant_parent_cursor(1)
nnoremap <silent><buffer><expr> <C-k> denite#incremant_parent_cursor(-1)

" vim: set ft=vim et sts=2 sw=2 ts=2 :
