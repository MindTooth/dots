nnoremap <silent><buffer><expr> <CR>
      \ denite#do_map('do_action')
nnoremap <silent><buffer><expr> <Tab>
      \ denite#do_map('choose_action')
nnoremap <silent><buffer><expr> p
      \ denite#do_map('do_action', 'preview')
nnoremap <silent><buffer><expr> q
      \ denite#do_map('quit')
nnoremap <silent><buffer><expr> i
      \ denite#do_map('open_filter_buffer')
nnoremap <silent><buffer><expr> <Space>
      \ denite#do_map('toggle_select').'j'
nnoremap <silent><buffer><expr> <C-v>
      \ denite#do_map('do_action', 'vsplit')
nnoremap <silent><buffer><expr> <C-s>
      \ denite#do_map('do_action', 'split')
nnoremap <silent><buffer><expr> <Esc>
      \ denite#do_map('quit')

" vim: set ft=vim et sts=2 sw=2 ts=2 :
