setlocal expandtab
setlocal shiftwidth=2
setlocal softtabstop=2

nnoremap <silent> <leader><leader>o :!open %<cr><cr>

" vim: set filetype=vim:
