" vimtex.vim

let g:vimtex_quickfix_mode=0
set conceallevel=2

let g:tex_flavor='latex'

" Set tectonic as the default TeX compiler engine'
let g:vimtex_compiler_method = 'tectonic'

" vim: set ft=vim et sts=2 sw=2 ts=2 :
