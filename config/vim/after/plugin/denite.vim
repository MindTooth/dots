scriptencoding utf-8

call denite#custom#option('default', {
      \ 'auto_resize': 1,
      \ 'statusline': 0,
      \ 'reversed': 0,
      \ 'match_highlight': 1,
      \ 'filter_updatetime': 100,
      \ 'start_filter': 1,
      \ 'prompt': 'λ ',
      \ })

" Add custom menus
" let s:menus = {}

" let s:menus.zsh = {
"       \ 'description': 'Edit your import zsh configuration'
"       \ }
" let s:menus.zsh.file_candidates = [
"       \ ['zshrc', '~/.config/zsh/.zshrc'],
"       \ ['zshenv', '~/.zshenv'],
"       \ ]

" call denite#custom#var('menu', 'menus', s:menus)

" call denite#custom#filter('matcher/ignore_globs', 'ignore_globs',
"       \ [
"       \   '*.bak',
"       \   '*.class',
"       \   '*.exe',
"       \   '*.min.*',
"       \   '*.o',
"       \   '*.pyc',
"       \   '*.sw[po]',
"       \   '*~',
"       \   '.DS_Store',
"       \   '.bzr/',
"       \   '.git/',
"       \   '.git/',
"       \   '.hg/',
"       \   '.ropeproject/',
"       \   '.svn/',
"       \   '__pycache__/',
"       \   '.venv/',
"       \   'fonts/',
"       \   'images/',
"       \   'img/',
"       \   '**/node_modules/',
"       \   '@node_modules',
"       \   'node_modules/',
"       \   '*node_modules/',
"       \   'tags',
"       \   'tags-*',
"       \   'venv/',
"       \ ])

if executable('rg')
  call denite#custom#var('file/rec', 'command',
        \ ['rg', '--files', '--glob', '!.git', '--color', 'never'])

  call denite#custom#var('grep', {
        \ 'command': ['rg'],
        \ 'default_opts': ['-i', '--vimgrep', '--no-heading'],
        \ 'recursive_opts': [],
        \ 'pattern_opt': ['--regexp'],
        \ 'separator': ['--'],
        \ 'final_opts': [],
        \ })
endif

nnoremap <silent><c-t> :DeniteProjectDir file/rec<cr>
nnoremap <silent><leader>sb :<C-u>Denite buffer<cr>
nnoremap <silent><leader>sw :<C-u>DeniteCursorWord grep:.<cr>
nnoremap <silent><leader>sp :<C-u>Denite grep:. -no-empty<CR>

" vim: set ft=vim et sts=2 sw=2 ts=2 :
