" goyo.vim - Tmux helper function
if has('autocmd')
	augroup goyo_tmux
		autocmd!

		autocmd User GoyoEnter nested call <SID>goyo_enter()
		autocmd User GoyoLeave nested call <SID>goyo_leave()
	augroup END
endif

function! s:goyo_tmux_toggle()
    if g:goyo_on
        silent !tmux set status on
        silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
    else
        silent !tmux set status on
        silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
    endif
endfunction

function! s:goyo_enter()
    :QuickScopeToggle
    if executable('tmux') && strlen($TMUX)
        silent !tmux set status off
        silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
    endif
    set nofoldenable
    set scrolloff=999
    let g:goyo_on = 1
endfunction

function! s:goyo_leave()
    :QuickScopeToggle
    if executable('tmux') && strlen($TMUX)
        silent !tmux set status on
        silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
    endif
    set foldenable
    set scrolloff=5
    let g:goyo_on = 0
endfunction

" vim: set filetype=vim:
