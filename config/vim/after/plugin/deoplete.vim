" deoplete.nvim

"augroup deoplete_close
"  autocmd!
"
"  autocmd CompleteDone * silent! pclose!
"augroup END

inoremap <expr><C-h>
      \ deoplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS>
      \ deoplete#smart_close_popup()."\<C-h>"

" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function() abort
  return pumvisible() ? deoplete#close_popup()."\<CR>" : "\<CR>"
endfunction

" Hidden autocomplete preview
"set completeopt-=preview

" vim: set ft=vim et sts=2 sw=2 ts=2 :
