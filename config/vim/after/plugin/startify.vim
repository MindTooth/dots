" startify.vim
let g:startify_commands = [
        \ {'m': ['<messages>', 'messages']},
        \ {'u': ['<update plugins>', 'call dein#update()']},
        \ ]
let g:startify_custom_header =
        \ 'map(startify#fortune#cowsay(), "\"     \".v:val")'
let g:startify_fortune_use_unicode = 1
let g:startify_padding_left = 8

function! StartifyEntryFormat() abort
	return 'nerdfont#find(absolute_path) ." ". entry_path'
endfunction

" vim: set filetype=vim:
