if has('macunix')
  " let g:llvm_bin_path_macos = '/usr/local/opt/llvm/bin'

  " Seems that this can be disabled.  Added to fix a `wchar`
  " issue.  Removed this fixed the bug in newer macOS.

  " let g:ale_c_clangformat_executable = llvm_bin_path_macos . '/clang-format'
  " let g:ale_cpp_clang_executable = llvm_bin_path_macos . '/clang++'
  " let g:ale_cpp_clangd_executable = llvm_bin_path_macos . '/clangd'
  " let g:ale_cpp_clangtidy_executable = llvm_bin_path_macos . '/clang-tidy'
endif

nnoremap <F7> :let <C-R>=g:ale_fix_on_save ? 'g:ale_fix_on_save=0' : 'g:ale_fix_on_save=1'<CR><CR>

" Disable ALE's LSP support for now.
let g:ale_disable_lsp = 1

" Change swift-format path for ALE fixer
let g:ale_swift_swiftformat_executable = 'swift-format'
let g:ale_swift_swiftformat_options = 'format -i'

let s:sourcekit_lsp_path = systemlist('xcrun --find sourcekit-lsp')
let g:ale_sourcekit_lsp_executable = s:sourcekit_lsp_path[0]

let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'

let g:ale_fixers = {
      \ '*': ['remove_trailing_lines', 'trim_whitespace'],
      \ 'c': ['clang-format'],
      \ 'cpp': ['clang-format'],
      \ 'css': ['prettier'],
      \ 'go': [],
      \ 'html': ['prettier'],
      \ 'htmldjango': ['prettier'],
      \ 'java': ['prettier'],
      \ 'javascript': ['prettier'],
      \ 'json': ['prettier'],
      \ 'php': ['php_cs_fixer'],
      \ 'python': ['black', 'ruff'],
      \ 'sass': ['prettier'],
      \ 'scss': ['prettier'],
      \ 'sh': ['shfmt'],
      \ 'swift': ['swiftformat'],
      \ 'terraform': ['terraform'],
      \ 'typescript': ['prettier'],
      \ 'yaml': ['prettier'],
      \ 'xml': ['xmllint'],
      \ 'xsd': ['xmllint'],
      \ }

let g:ale_linters = {
      \ 'go': [],
      \ 'python': [],
      \ }

let g:ale_fix_on_save = 1

" vim: set ft=vim et sts=2 sw=2 ts=2 :
