" lsp.vim
scriptencoding utf-8

" Debugging
let enable_lsp_debugging = 0
if enable_lsp_debugging
  let g:lsp_log_verbose = 1
  let g:lsp_log_file = expand('~/vim-lsp.log')

  let g:deoplete#sources#vim_lsp#log = expand('~/deoplete-vim-lsp.log')
endif


" Diagnostics
let g:lsp_signs_enabled = 1
let g:lsp_diagnostics_enabled = 0
let g:lsp_diagnostics_echo_cursor = 1
let g:lsp_virtual_text_enabled = 1
let g:lsp_signs_error = {'text': '✗'}
let g:lsp_signs_warning = {'text': '‼'}
let g:lsp_signs_information = {'text': 'i'}
let g:lsp_signs_hint = {'text': '?'}
let g:lsp_virtual_text_prefix = ' ‣ '
let g:lsp_diagnostics_float_cursor = 0
let g:lsp_diagnostics_echo_delay = 200

" Highlight
let g:lsp_highlights_enabled = 0
let g:lsp_highlight_references_enabled = 0

augroup register_lsp_clients
  autocmd!

  " https://qiita.com/lighttiger2505/items/3065164798bc9cd615d4
  let s:clangd_path = systemlist('xcrun --find clangd')
  let s:sourcekit_lsp_path = systemlist('xcrun --find sourcekit-lsp')

  if executable('pyright-langserver')
    autocmd User lsp_setup call lsp#register_server({
          \ 'name': 'pyright-langserver',
          \ 'cmd': {server_info->['pyright-langserver', '--stdio']},
          \ 'allowlist': ['python'],
          \ 'root_uri': {server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), 'pyrightconfig.json'))},
          \ 'workspace_config': {
          \   'python': {
          \     'analysis': {
          \       'useLibraryCodeForTypes': v:true,
          \     },
          \  },
          \ }
          \ })
  endif

  " if executable('ruff')
  "   autocmd User lsp_setup call lsp#register_server({
  "         \ 'name': 'ruff-lsp',
  "         \ 'cmd': {server_info->['ruff', 'server', '--preview']},
  "         \ 'allowlist': ['python'],
  "         \ 'workspace_config': {'configurationPreference': 'filesystemFirst'},
  "         \ })
  " endif

  if executable(s:sourcekit_lsp_path[0])
    autocmd User lsp_setup call lsp#register_server({
          \ 'name': 'sourcekit-lsp',
          \ 'cmd': {server_info->[s:sourcekit_lsp_path[0]]},
          \ 'allowlist': ['c', 'cpp', 'objc', 'objcpp', 'swift'],
          \ })
  endif

  if executable('terraform-ls')
    autocmd User lsp_setup call lsp#register_server({
          \ 'name': 'terraform-ls',
          \ 'cmd': {server_info->['terraform-ls', 'serve', '-log-file=/tmp/terraform-ls-{{pid}}.log']},
          \ 'root_uri': { server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_directory(lsp#utils#get_buffer_path(), '.git/..'))},
          \ 'allowlist': ['terraform'],
          \ })
  endif

  if executable('tailwindcss-intellisense')
    autocmd User lsp_setup call lsp#register_server({
          \ 'name': 'tailwindcss-intellisense',
          \ 'cmd': {server_info->[&shell, &shellcmdflag, 'tailwindcss-language-server --stdio']},
          \ 'config': {
          \   'refresh_pattern': '\(@[a-zA-Z0-9_]*\|[a-zA-Z0-9_-]\+\)$',
          \ },
          \ 'root_uri': {server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), 'tailwind.config.js'))},
          \ 'allowlist': ['css', 'html', 'htmldjango'],
          \ })
  endif

  if executable('typescript-language-server')
    autocmd User lsp_setup call lsp#register_server({
          \ 'name': 'typescript-language-server',
          \ 'cmd': {server_info->[&shell, &shellcmdflag, 'typescript-language-server --stdio']},
          \ 'root_uri': { server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_directory(lsp#utils#get_buffer_path(), '.git/..'))},
          \ 'allowlist': ['javascript', 'javascript.jsx', 'javascriptreact', 'typescript']
          \ })
  endif

  if executable('vscode-html-language-server')
    autocmd User lsp_setup call lsp#register_server({
          \ 'name': 'vscode-html-language-server',
          \ 'cmd': {server_info->[&shell, &shellcmdflag, 'vscode-html-language-server --stdio']},
          \ 'root_uri': { server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_directory(lsp#utils#get_buffer_path(), '.git/..'))},
          \ 'allowlist': ['html', 'htmldjango'],
          \ })
  endif

  if executable('vscode-json-language-server')
    autocmd User lsp_setup call lsp#register_server({
          \ 'name': 'vscode-json-languageserver',
          \ 'cmd': {server_info->[&shell, &shellcmdflag, 'vscode-json-language-server --stdio']},
          \ 'root_uri': { server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_directory(lsp#utils#get_buffer_path(), '.git/..'))},
          \ 'allowlist': ['json', 'jsonc'],
          \ 'config': {
          \   'refresh_pattern': '\("\k*\|\[\|\k\+\)$',
          \ },
          \ })
  endif

  if executable('texlab')
    autocmd User lsp_setup call lsp#register_server({
          \ 'name': 'texlab',
          \ 'cmd': {server_info->['texlab']},
          \ 'allowlist': ['bib', 'plaintex', 'tex'],
          \ })
  endif

  if executable('gopls')
    autocmd User lsp_setup call lsp#register_server({
          \ 'name': 'gopls',
          \ 'cmd': {server_info->['gopls']},
          \ 'allowlist': ['go'],
          \ })

    autocmd BufWritePre *.go LspDocumentFormatSync
  endif

  if executable('yaml-language-server')
    autocmd User lsp_setup call lsp#register_server({
          \ 'name': 'yaml-language-server',
          \ 'cmd': {server_info->['yaml-language-server', '--stdio']},
          \ 'root_uri': { server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_directory(lsp#utils#get_buffer_path(), '.git/..'))},
          \ 'allowlist': ['yaml', 'yaml.ansible'],
          \ 'workspace_config': {
          \   'yaml': {
          \     'completion': v:true,
          \     'customTags': ["!reference sequence"],
          \     'hover': v:true,
          \     'schemas': {},
          \     'validate': v:true,
          \   },
          \ },
          \ })
  endif

  " if executable('yaml-language-server')
  "   autocmd User lsp_setup call lsp#register_server({
  "         \ 'name': 'yaml-language-server',
  "         \ 'cmd': {server_info->['yaml-language-server', '--stdio']},
  "         \ 'root_uri': { server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_directory(lsp#utils#get_buffer_path(), '.git/..'))},
  "         \ 'allowlist': ['yaml', 'yaml.ansible'],
  "         \ 'workspace_config': {
  "         \   'yaml': {
  "         \     'validate': v:true,
  "         \     'hover': v:true,
  "         \     'completion': v:true,
  "         \     'customTags': ["!reference sequence"],
  "         \     'schemas': {},
  "         \     'schemaStore': { 'enable': v:true },
  "         \   },
  "         \ },
  "         \ })
  " endif
augroup END


function! s:on_lsp_buffer_enabled() abort
  setlocal signcolumn=yes

  if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif

  nmap <buffer> gd <plug>(lsp-definition)
  nmap <buffer> gr <plug>(lsp-references)
  nmap <buffer> gi <plug>(lsp-implementation)
  nmap <buffer> gt <plug>(lsp-type-definition)
  nmap <buffer> <leader>rn <plug>(lsp-rename)
  nmap <buffer> [g <Plug>(lsp-previous-diagnostic)
  nmap <buffer> ]g <Plug>(lsp-next-diagnostic)
  nmap <buffer> K <plug>(lsp-hover)

  " refer to doc to add more commands
endfunction

augroup lsp_install
  autocmd!
  " call s:on_lsp_buffer_enabled only for languages that has the server registered.
  autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END

set omnifunc=lsp#complete

" vim: set ft=vim et sts=2 sw=2 ts=2 :
