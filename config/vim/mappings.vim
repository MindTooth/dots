" mappings.vim

" Setting <leader> early so mappings take effect from this point going
" forward.  If not set here, mappings for <leader>-key don't get
" applied.
let g:mapleader = "\<Space>"
let g:maplocalleader="\\"

" dein.vim
nnoremap <silent> <leader>dr :call mindtooth#dein_recache()<CR>
nnoremap <silent> <leader>du :call dein#update()<CR>

" Key Mappings {{{

" Don't use Ex mode, use Q for formatting.
" Revert with ":unmap Q".
map Q gq

" Visual select line
nnoremap <leader><leader> V

" https://www.linuxfordevices.com/tutorials/linux/copy-paste-multiple-registers-in-vim
" Set a key-mapping for copy and pasting to the system clipboard
vnoremap <Leader>y "+y
nnoremap <Leader>p "+p

" Set key-mapping for dealing with two alphabetical registers easily
" Two does the work for me, you can set more

vnoremap <Leader>a "ay
vnoremap <Leader>A "Ay
nnoremap <Leader>a "ap

vnoremap <Leader>x "xy
vnoremap <Leader>X "Xy
nnoremap <Leader>x "xp

" Quit
" nnoremap <leader><leader>e :quit<CR>
nnoremap <leader>q :quit<CR>
" inoremap <leader><leader>q <esc>:quit<CR>

" Write
" nnoremap <leader><leader>s :write<CR>
nnoremap <leader>w :write<CR>
" inoremap <leader><leader>w <esc>:write<CR>

" Exit
" nnoremap <leader><leader>x :exit<CR>
nnoremap <leader>xx :exit<CR>
" inoremap <leader><leader>x <esc>:exit<CR>

" Fix sudo
cnoremap w!! w !sudo tee % >/dev/null

" Make jk escape instead of double jjs
inoremap jk <esc>
inoremap jK <esc>
inoremap Jk <esc>
inoremap JK <esc>

" http://stackoverflow.com/questions/20975928/moving-the-cursor-through-long-soft-wrapped-lines-in-vim/21000307#21000307
noremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
noremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')

noremap H 0
noremap L $

" Search mappings: These will make it so that going to the next one in a
" search will center on the line it's found in.
nnoremap n nzzzv
nnoremap N Nzzzv

" Clear highlight
nnoremap <silent> <BS> :nohlsearch<CR>

" Clear Arrows {{
noremap <Left> <Nop>
noremap <Right> <Nop>
noremap <Up> <Nop>
noremap <Down> <Nop>

" }}

" IPA Mapping
nnoremap <leader><leader>i :call ToggleIPA()<CR>
inoremap <leader><leader>i <esc>:call ToggleIPA()<CR>a

" Format paragraph
nnoremap <leader>fp vipgq

" Copy buffer

function! CopyToClipboard() abort
	let l:pos = winsaveview()
	normal! ggVGy
	call winrestview(pos)
endfunction

nnoremap <leader>cc :call CopyToClipboard()<CR>

" vim: set filetype=vim:
