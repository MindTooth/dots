" dein.vim - plugins

" Referance on dein sourcing order
" https://qiita.com/delphinus/items/cd221a450fd23506e81a

set runtimepath+=~/.cache/dein/repos/github.com/shougo/dein.vim

let s:path = expand('~/.cache/dein')
if !dein#load_state(s:path)
  finish
endif

call dein#begin('~/.cache/dein', [expand('<sfile>')])
call dein#add('~/.cache/dein/repos/github.com/shougo/dein.vim')

" User Plugins
call dein#add('bfrg/vim-cpp-modern')
call dein#add('cespare/vim-toml')
call dein#add('chrisbra/unicode.vim')
call dein#add('dag/vim-fish.git')
call dein#add('dhruvasagar/vim-table-mode')
call dein#add('dstein64/vim-startuptime')
if !has('nvim')
  call dein#add('editorconfig/editorconfig-vim')
endif
call dein#add('elzr/vim-json')
call dein#add('fatih/vim-go')
call dein#add('google/vim-jsonnet')
call dein#add('hashivim/vim-hashicorp-tools')
call dein#add('haya14busa/vim-asterisk')
call dein#add('honza/vim-snippets')
call dein#add('itchyny/vim-cursorword')
call dein#add('junegunn/goyo.vim')
call dein#add('junegunn/vim-easy-align')
call dein#add('keith/swift.vim')
call dein#add('knubie/vim-kitty-navigator')
call dein#config('vim-kitty-navigator', {
      \ 'merged': 0
      \ })
call dein#add('lambdalisue/fern.vim')
call dein#add('lambdalisue/fern-renderer-nerdfont.vim')
call dein#add('lambdalisue/nerdfont.vim')
call dein#add('lervag/vimtex')
call dein#add('lighttiger2505/deoplete-vim-lsp')
call dein#add('markonm/traces.vim')
call dein#add('mattn/emmet-vim')
call dein#add('mhinz/vim-signify')
call dein#add('mhinz/vim-startify')
call dein#add('ntpeters/vim-better-whitespace')
call dein#add('noprompt/vim-yardoc')
call dein#add('normen/vim-pio')
call dein#add('othree/html5.vim')
call dein#add('pboettch/vim-cmake-syntax')
call dein#add('ponko2/deoplete-fish')
call dein#add('prabirshrestha/vim-lsp')
call dein#add('rodjek/vim-puppet')
if !has('nvim')
  call dein#add('roxma/nvim-yarp')
  call dein#add('roxma/vim-hug-neovim-rpc')
endif
call dein#add('srcery-colors/srcery-vim', {
    \ 'merged': 0
    \ })
call dein#add('shougo/context_filetype.vim')
call dein#add('shougo/denite.nvim')
call dein#add('shougo/deoplete.nvim')
call dein#add('shougo/echodoc.vim')
call dein#add('shougo/neco-syntax')
call dein#add('shougo/neco-vim')
call dein#add('shougo/neoinclude.vim')
call dein#add('shougo/neosnippet-snippets')
call dein#add('shougo/neosnippet.vim')
call dein#add('stephpy/vim-yaml')
call dein#add('thomasfaingnaert/vim-lsp-neosnippet')
call dein#add('thomasfaingnaert/vim-lsp-snippets')
call dein#add('tmux-plugins/vim-tmux')
call dein#add('tpope/vim-commentary')
call dein#add('tpope/vim-dadbod')
call dein#add('tpope/vim-endwise')
call dein#add('tpope/vim-eunuch')
call dein#add('tpope/vim-fugitive')
call dein#add('tpope/vim-repeat')
call dein#add('tpope/vim-sensible')
call dein#add('tpope/vim-surround')
call dein#add('tpope/vim-unimpaired')
call dein#add('tpope/vim-vinegar')
call dein#add('unblevable/quick-scope')
call dein#add('cdelledonne/vim-cmake')
call dein#add('vim-airline/vim-airline')
call dein#add('vim-pandoc/vim-pandoc')
call dein#add('vim-pandoc/vim-pandoc-syntax')
call dein#add('w0rp/ale')
call dein#add('wincent/terminus')
call dein#add('yggdroot/indentline')

call dein#config('srcery-vim', {
      \ 'merged': 0,
      \ })

" Package Config {{{

call dein#config('vim-toml', {
      \ 'on_ft': ['toml'],
      \ 'rev': 'main'
      \ })

call dein#config('swift.vim', {
      \ 'on_ft': ['swift'],
      \ })

call dein#config('vim-go', {
      \ 'merged': 0,
      \ 'hook_add': join([
      \   'let g:go_code_completion_enabled = 0',
      \   'let g:go_gopls_enabled = 0',
      \   'let g:go_imports_autosave = 1',
      \ ], "\n"),
      \ })

call dein#config('vim-table-mode', {
      \ 'on_ft': ['md', 'pandoc'],
      \ 'hook_add': join([
      \   'let g:table_mode_corner = "|"',
      \   'let g:table_mode_corner_corner = "|"',
      \   'let g:table_mode_disable_mappings = 1',
      \   'let g:table_mode_disable_tableize_mappings = 1',
      \   'let g:table_mode_header_fillchar = "-"',
      \ ], "\n")
      \ })

call dein#config('vim-json', {
      \ 'on_ft': ['json'],
      \ 'hook_add': '
      \   let g:vim_json_syntax_conceal = 0
      \ ',
      \ })

call dein#config('vimtex', {
      \ 'on_ft': ['bib', 'tex', 'plaintex'],
      \ })

call dein#config('vim-pandoc', {
      \ 'merged': 0,
      \ 'hook_add': '
      \   let g:pandoc#spell#enabled = 0
      \ '
      \ })

call dein#config('vim-pandoc-syntax', {
      \ 'merged': 0,
      \ })

call dein#config('vim-tmux', {
      \ 'on_ft': ['tmux'],
      \ })

call dein#config('vim-yaml', {
      \ 'on_ft': ['yaml'],
      \ })

call dein#config('neosnippet.vim', {
      \ 'lazy': v:true,
      \ 'on_event': 'InsertEnter',
      \ 'depends': ['neosnippet-snippets', 'context_filetype.vim'],
      \ 'hook_add': join([
      \   'let g:neosnippet#enable_snipmate_compatibility = 1',
      \   'let g:neosnippet#snippets_directory="~/.vim/snippets"',
      \   'let g:neosnippet#enable_completed_snippet = 1',
      \   'let g:neosnippet#enable_complete_done = 1',
      \   'set completeopt-=preview',
      \   'imap <C-e> <Plug>(neosnippet_expand_or_jump)',
      \   'nmap <C-e> <Plug>(neosnippet_expand_or_jump)',
      \   'smap <C-e> <Plug>(neosnippet_expand_or_jump)',
      \   'xmap <C-e> <Plug>(neosnippet_expand_target)',
      \ ], "\n")
      \ })

call dein#config('vim-cmake-syntax', {
      \ 'on_ft': ['cmake'],
      \ })

call dein#config('emmet-vim', {
      \ 'on_ft': ['css', 'html', 'htmldjango'],
      \ })

call dein#config('html5.vim', {
      \ 'on_ft': ['css', 'html'],
      \ })

call dein#config('vim-lsp', {
      \ 'lazy': v:true,
      \ 'on_ft': [
      \   'bib',
      \   'c',
      \   'cpp',
      \   'go',
      \   'html',
      \   'htmldjango',
      \   'javascript',
      \   'javascript.jsx',
      \   'javascriptreact',
      \   'json',
      \   'json5',
      \   'plaintex',
      \   'python',
      \   'swift',
      \   'terraform',
      \   'tex',
      \   'typescript',
      \   'yaml',
      \   'yaml.ansible',
      \ ],
      \ })

" Some tips on setting up deoplete and lsp
" https://qiita.com/lighttiger2505/items/3065164798bc9cd615d4

call dein#config('deoplete.nvim', {
      \ 'merged': 0,
      \ 'lazy': v:true,
      \ 'depends': 'context_filetype.vim',
      \ 'on_event': 'InsertEnter',
      \ 'hook_source': "
      \   let s:use_lsp_sources = ['lsp', 'neosnippet', 'dictionary', 'file'] \n
      \   let g:deoplete#enable_at_startup = v:true \n
      \   set completeopt-=preview \n
      \   call deoplete#custom#option({
      \     'auto_preview': v:true,
      \     'min_pattern_length': 1,
      \     'num_processes': 2,
      \     'omni_patterns': {
      \       'pandoc': '@',
      \     },
      \     'refresh_always': v:false,
      \     'skip_multibyte': v:true,
      \     'sources': {
      \       'c': s:use_lsp_sources,
      \       'cpp': s:use_lsp_sources,
      \       'go': s:use_lsp_sources,
      \       'html': s:use_lsp_sources,
      \       'htmldjango': s:use_lsp_sources,
      \       'javascript': s:use_lsp_sources,
      \       'json': s:use_lsp_sources,
      \       'json5': s:use_lsp_sources,
      \       'swift': s:use_lsp_sources,
      \       'terraform': s:use_lsp_sources,
      \       'tex': s:use_lsp_sources,
      \       'typescript': s:use_lsp_sources,
      \       'yaml': s:use_lsp_sources,
      \     },
      \   }) \n
      \   call deoplete#custom#source('_', 'converters', [
      \     'converter_remove_paren',
      \     'converter_remove_overlap',
      \     'converter_truncate_abbr',
      \     'converter_truncate_menu',
      \     'converter_auto_delimiter',
      \   ])
      \ ",
      \ })

      " \   call deoplete#custom#source('lsp', 'dup', v:false) \n
call dein#config('deoplete-vim-lsp', {
      \ 'lazy': v:true,
      \ 'on_event': 'InsertEnter',
      \ 'depends': ['deoplete.nvim', 'vim-lsp'],
      \ })

call dein#config('deoplete-fish', {
      \ 'on_ft': ['fish']
      \ })

call dein#config('indentline', {
      \ 'hook_add': join([
      \   'let g:indentLine_defaultGroup = "Comment"',
      \   'let g:indentLine_enabled = 1',
      \   'let g:indentLine_setColors = 1',
      \ ], "\n")
      \ })

" }}}

call dein#end()
call dein#save_state()

" vim: set ft=vim et sts=2 sw=2 ts=2 :
