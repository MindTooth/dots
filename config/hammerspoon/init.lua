-- https://spinscale.de/posts/2016-11-08-creating-a-productive-osx-environment-hammerspoon.html

--[[ function factory that takes the multipliers of screen width
and height to produce the window's x pos, y pos, width, and height ]]
function baseMove(x, y, w, h)
    return function()
        local win = hs.window.focusedWindow()
        local f = win:frame()
        local screen = win:screen()
        local max = screen:frame()

        -- add max.x so it stays on the same screen, works with my second screen
        f.x = max.w * x + max.x
        f.y = max.h * y
        f.w = max.w * w
        f.h = max.h * h
        win:setFrame(f, 0)
    end
end

-- feature spectacle/another window sizing apps
hs.hotkey.bind({'ctrl', 'alt', 'cmd'}, 'Left', baseMove(0, 0, 0.5, 1))
hs.hotkey.bind({'ctrl', 'alt', 'cmd'}, 'Right', baseMove(0.5, 0, 0.5, 1))
hs.hotkey.bind({'ctrl', 'alt', 'cmd'}, 'Down', baseMove(0, 0.5, 1, 0.5))
hs.hotkey.bind({'ctrl', 'alt', 'cmd'}, 'Up', baseMove(0, 0, 1, 0.5))
hs.hotkey.bind({'ctrl', 'alt', 'cmd'}, '1', baseMove(0, 0, 0.5, 0.5))
hs.hotkey.bind({'ctrl', 'alt', 'cmd'}, '2', baseMove(0.5, 0, 0.5, 0.5))
hs.hotkey.bind({'ctrl', 'alt', 'cmd'}, '3', baseMove(0, 0.5, 0.5, 0.5))
hs.hotkey.bind({'ctrl', 'alt', 'cmd'}, '4', baseMove(0.5, 0.5, 0.5, 0.5))
hs.hotkey.bind({'ctrl', 'alt', 'cmd'}, 'M', hs.grid.maximizeWindow)


-- lock screen shortcut
hs.hotkey.bind({'ctrl', 'alt', 'cmd'}, 'L', function() hs.caffeinate.lockScreen() end)


-- quick jump to important applications
hs.grid.setMargins({0, 0})
hs.hotkey.bind({'ctrl', 'alt', 'cmd'}, 'B', function ()
    hs.application.launchOrFocus("Safari")
    hs.application.launchOrFocus("Mail")
    hs.application.launchOrFocus("Kitty")
end)
hs.hotkey.bind({'ctrl', 'alt', 'cmd'}, 'S', function () hs.application.launchOrFocus("Safari") end)
hs.hotkey.bind({'ctrl', 'alt', 'cmd'}, 'E', function () hs.application.launchOrFocus("Mail") end)
hs.hotkey.bind({'ctrl', 'alt', 'cmd'}, 'T', function () hs.application.launchOrFocus("Kitty") end)


-- maximize firefox
firefox = hs.window.filter.new{'Firefox'}
firefox
    :subscribe(hs.window.filter.windowCreated, function ()
        hs.application.get('Firefox'):mainWindow():maximize()
    end)


-- Test Dialog
-- hs.hotkey.bind({'ctrl', 'alt', 'cmd'}, 'G', function()
-- 	hs.dialog.textPrompt("Main message.", "Please enter something:")
-- end)

-- hs.hotkey.bind({'ctrl', 'alt', 'cmd'}, 'O', function()
-- 	hs.focus();
-- 	s1,s2 = hs.dialog.textPrompt("Main message.", "Please enter something:", "Default Value", "OK")
-- 	hs.alert.show(s2)
-- end)

-- reload config
function reloadConfig(files)
    doReload = false
    for _,file in pairs(files) do
        if file:sub(-4) == ".lua" then
            doReload = true
        end
    end
    if doReload then
        hs.reload()
    end
end
myWatcher = hs.pathwatcher.new(os.getenv("HOME") .. "/.hammerspoon/", reloadConfig):start()
hs.alert.show("Config loaded")
