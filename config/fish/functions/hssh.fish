function hssh
  set -l hosts (host-lookup $argv)

  kitty +kitten ssh (printf "%s\n" $hosts | gum filter)
end
