function figlet
  if not command -sq figlet
    echo "Need figlet binary"
  end

  if test (count $argv) -lt 2
    return 1
  end


  command figlet -d ~/.local/share/figlet/fonts/ -f "$argv[1]" "$argv[2]"

end
