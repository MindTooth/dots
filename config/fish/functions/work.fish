function work
  if test -z $WORK_ENABLED
    cd $WORK_PATH
    echo -e "\nLoading the work directory!\n"
    return
  end

  eval (ssh-agent -a $SSH_AUTH_SOCK -c)

  # if [ ! -e "$SSH_AUTH_SOCK" ]; then
    # direnv_load ssh-agent -a $SSH_AUTH_SOCK -- direnv dump
    # eval $(ssh-agent -a "$SSH_AUTH_SOCK" >/dev/null || true)

    # ssh-agent -a $SSH_AUTH_SOCK
  # fi
  # echo $argv
  # argparse --name=my_function 'h/help' 'n/name=' -- $argv

  # if test -o \((count $argv) -le 0 \)
  set -l args_count (count $argv)

  if test \( $args_count -le 0 \) -a \( (pwd) != $WORK_PATH \)
    echo -e "\nReturning to the work directory!\n"
    cd $WORK_PATH
    return
  end

  switch $argv[1]
    case "git"
      cd $WORK_PATH/Git/(ls $WORK_PATH/Git | fzf)
  end
end
