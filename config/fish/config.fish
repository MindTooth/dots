# ~/.config/fish/config.fish
# Maintainer: Birger J. Nordølum (birgerjn (at) gmail dot com)
#
# https://fishshell.com/docs/current/index.html#initialization-files

if not status --is-interactive
    exit 0
end

# Remove greeting
set -gx fish_greeting ''


# Set lang to en_US.UTF-8
set -x LANG en_US.UTF-8
set -x LC_ALL $LANG
set -x LC_CTYPE $LANG

# XDG Base Folders
set -x XDG_CACHE_HOME $HOME/.cache
set -x XDG_CONFIG_HOME $HOME/.config
set -x XDG_DATA_HOME $HOME/.local/share

# set -x SHELL (brew --prefix)/bin/fish
set -x SHELL /opt/homebrew/bin/fish

set -l arch $(arch)
set path $HOME/.config/fish

# if status --is-login
# source $path/env.fish

# Instead add the paths inside `/etc/paths.d/homebrew`.
# if test $arch = arm64
#     fish_add_path /opt/homebrew/sbin
#     fish_add_path /opt/homebrew/bin
# end
#
# eval (/opt/homebrew/bin/brew shellenv fish)
set -gx HOMEBREW_PREFIX "/opt/homebrew";
set -gx HOMEBREW_CELLAR "/opt/homebrew/Cellar";
set -gx HOMEBREW_REPOSITORY "/opt/homebrew";
# ! set -q PATH; and set PATH ''; set -gx PATH "/opt/homebrew/bin" "/opt/homebrew/sbin" $PATH;
! set -q MANPATH; and set MANPATH ''; set -gx MANPATH "/opt/homebrew/share/man" $MANPATH;
! set -q INFOPATH; and set INFOPATH ''; set -gx INFOPATH "/opt/homebrew/share/info" $INFOPATH;


# Using GPG instead of SSH
# gpgconf --kill gpg-agent
#set -x SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
set -e SSH_AUTH_SOCK
# set -U -x SSH_AUTH_SOCK ~/.gnupg/S.gpg-agent.ssh
set -U -x SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
# gpgconf --launch gpg-agent
# end

# source $path/aliases.fish

# Load direnv
if command -sq direnv
    direnv hook fish | source
end

if command -sq zoxide
    zoxide init --cmd cd fish | source
end

# fish_add_path /usr/local/opt/openssl@3/bin

# pnpm
set -gx PNPM_HOME "/Users/birgerjn/.local/share/pnpm"
if not string match -q -- $PNPM_HOME $PATH
  set -gx PATH "$PNPM_HOME" $PATH
end

# pnpm end
# tabtab source for packages
# uninstall by removing these lines
[ -f ~/.config/tabtab/fish/__tabtab.fish ]; and . ~/.config/tabtab/fish/__tabtab.fish; or true

set -gx __fish_initialized 1
