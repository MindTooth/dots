# Reload config.fish to activate changes
alias rf 'source ~/.config/fish/config.fish'

alias vim nvim

alias evalssh 'eval (ssh-agent -c -a $SSH_AUTH_SOCK)'

function reloadConfigMessage -d "Print source message"
    printf "\n\t%s\n" $argv
end

# Fast config edit
function e -w nvim -d 'NeoVim shorthand'
    command nvim $argv
end
function g -w git -d 'Git shorthand'
    command git $argv
end
function t -w tmux -d 'tmux shorthand'
    command tmux $argv
end

function ea -d 'Edit fish alias'
    command nvim ~/.config/fish/conf.d/aliases.fish
    if test $status -eq 0
        source ~/.config/fish/conf.d/aliases.fish
        reloadConfigMessage "Reloading fish aliases"
    end
end

function ef -d 'Edit fish'
    command nvim ~/.config/fish/config.fish
    if test $status -eq 0
        source ~/.config/fish/config.fish
        reloadConfigMessage "Reloading fish.conf"
    end
end

alias eg '$EDITOR ~/.config/git/config'
alias ek '$EDITOR ~/.config/kitty/kitty.conf'
alias en '$EDITOR ~/.config/newsboat/config'
alias et '$EDITOR ~/.tmux.conf'
alias ev '$EDITOR ~/.vimrc'

if command -sq kitty
    alias kssh="kitty +kitten ssh"
end

if command -sq bat
    alias cat="bat --plain"
end

if command -sq kubectl
    alias k="kubectl"
    complete -c k -w kubectl
end

alias respad 'defaults write com.apple.dock ResetLaunchPad -bool true; killall Dock'

alias q exit

alias cdc 'cd && clear'

if status --is-interactive
    set -g fish_user_abbreviations
    abbr --add --global brwe brew
    abbr --add --global bri 'brew install'
    abbr --add --global brii 'brew info'
    abbr --add --global src 'pushd ~/Source'
    abbr --add --global brii 'brew info'
    abbr --add --global brl 'brew list -1'
    abbr --add --global brr 'brew remove'
    abbr --add --global brs 'brew search'
    abbr --add --global brci 'brew cask install'
    abbr --add --global brcii 'brew cask info'
    abbr --add --global brcl 'brew cask list -1'
    abbr --add --global brcr 'brew cask uninstall'
    abbr --add --global nb newsboat
    abbr --add --global vi vim
    abbr --add --global cdd 'cd ~/.dots'
end

complete -c workpass -w pass

complete -c thruk -w ssh

# https://omarelhawary.me/blog/faster-directory-navigation-with-fzf
alias gcd "cd $HOME/Work/Git/ && cd (find $HOME/Work/Git -type d -depth 1 -not -path '*/.*' | sed 's:.*/::' | sort | gum filter)"

alias ggr "cd (git rev-parse --show-toplevel)"

# Check weather
alias wttr "curl wttr.in/Oslo\?2\?n\?Q 2>/dev/null | sed -e :a -e '$d;N;2,2ba' -e 'P;D'"
