# General Environment Settings {{{

# k0sctl
set -x DISABLE_TELEMETRY true

# ice
set -x ICE_PATH $HOME/ice

# WORK
set -x WORK_PATH $HOME/Work

# Editor/Pager
if command -sq nvim
    set DEFAULT_EDITOR nvim
else
    set DEFAULT_EDITOR vim
end

set -x EDITOR $DEFAULT_EDITOR
set -x VISUAL $DEFAULT_EDITOR

# }}}
# Path Settings {{{

# Go
if command -sq go
    set -x GOPATH $HOME/.go

    fish_add_path $GOPATH/bin
end

# Cargp - Rust
if command -sq $HOME/.cargo/bin/rustc
    fish_add_path $HOME/.cargo/bin
end

# pipx - Python
if command -sq pipx
    fish_add_path $HOME/.local/bin
end

# perlbrew
# if command -sq perlbrew
#     set -x PERLBREW_ROOT $HOME/.local/perlbrew
#     source $HOME/.local/perlbrew/etc/perlbrew.fish
# end

set -x PIPX_DEFAULT_PYTHON /usr/bin/python3

# Scripts
#set PATH $HOME/.bin $PATH
fish_add_path $HOME/.bin

# Add MySQL
# fish_add_path /usr/local/opt/mysql-client/bin

# }}}
# Homebrew Settings {{{

# Set access token in ~/.homebrew_token
# set -x HOMEBREW_GITHUB_API_TOKEN "TOKEN"
if test -e ~/.homebrew_token
    source ~/.homebrew_token
end

# set -x DOCKER_DEFAULT_PLATFORM linux/amd64

# set -x HOMEBREW_DEVELOPER true
# set -x HOMEBREW_INSTALL_CLEANUP true
# set -x HOMEBREW_NO_INSECURE_REDIRECT true
# set -x HOMEBREW_NO_INSTALL_FROM_API true
# set -x HOMEBREW_NO_ENV_HINTS true

# }}}
