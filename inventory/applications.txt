1Password 7.app
AdGuard for Safari.app
Alfred 4.app
AppCleaner.app
BetterSnapTool.app
Boom 3D.app
CleanMyDrive 2.app
CleanMyMac X.app
Cyberduck.app
DaisyDisk.app
DevCleaner.app
Docker.app
DuckDuckGo Privacy Essentials.app
Firefox.app
Hammerspoon.app
JetBrains Toolbox.app
Karabiner-Elements.app
Karabiner-EventViewer.app
KeepingYouAwake.app
Keybase.app
KnockKnock.app
Microsoft OneNote.app
Microsoft Remote Desktop.app
Moom.app
Notability.app
OneDrive.app
OnyX.app
OverSight.app
Pixelmator.app
Plex Media Server.app
Safari.app
Steam.app
SteelSeries Engine 3
Termius.app
Utilities
VirtualBox.app
Xcode.app
YubiKey Manager.app
YubiKey Personalization Tool.app
kitty.app
